
//  count the total number of fruits on sale.

	db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {
		    _id:"$onSale",
		    totalFruitsOnSale: {$count: {}}}},
		{ $project: {_id:0}}
	]);

// count the total number of fruits with stock more than 20

	db.fruits.aggregate([
	    {$match: {stock: {$gt: 20}}},
	    {$group: {
	    	_id:"$stock",
	    	stock20AndUp: {$count: {}}}},
	    { $project: {_id:0}}
	  ]);

//  Use $avg to get the average price of fruits onSale per supplier.

		db.fruits.aggregate([
		    {$match: {onSale: true}},
		    {$group: {
		    	_id:"$supplier_id",
		    	averagePrice: {$avg: "$price"}}},
		    { $project: {_id:0}}
		  ]);

// Use $max to get the highest price of a fruit per supplier.

		db.fruits.aggregate([
		    {$group: {
		    	_id:"$supplier_id",
		    	highestPrice: {$max: "$price"}}},
		    { $project: {_id:0}}
		  ]);

// Use $min to get the lowest price of a fruit per supplier.
		
		db.fruits.aggregate([
		    {$group: {
		    	_id:"$supplier_id",
		    	lowestPrice: {$min: "$price"}}},
		    { $project: {_id:0}}
		  ]);